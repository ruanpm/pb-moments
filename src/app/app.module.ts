import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { HomePage } from '../pages/home/home';
import { VideosPage } from '../pages/videos/videos';
import { StorePage } from '../pages/store/store';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { AdMobPro } from '@ionic-native/admob-pro';

import { SocialSharing } from '@ionic-native/social-sharing';
import { IonicAudioModule, WebAudioProvider, CordovaMediaProvider, defaultAudioProviderFactory } from 'ionic-audio';
import { HttpModule } from '@angular/http';

import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';

export function myCustomAudioProviderFactory() {
  return (window.hasOwnProperty('cordova')) ? new CordovaMediaProvider() : new WebAudioProvider();
}

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    VideosPage,
    StorePage,
    TabsPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicAudioModule.forRoot(defaultAudioProviderFactory), 
    // or use a custom provided function shown above myCustomAudioProviderFactory
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    VideosPage,
    StorePage,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AdMobPro,
    SocialSharing,
    YoutubeVideoPlayer,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
