import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import {DomSanitizer} from '@angular/platform-browser';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';

/**
 * Generated class for the VideosPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-videos',
  templateUrl: 'videos.html',
})
export class VideosPage {

	MAX_RESULT_OF_VIDEOS = 5;

	// Got it on google console
	GOOGLE_API_KEY = 'AIzaSyCOS315Pq00j7BMrD-mAMlhQyzLT5NnhPA';

	// queery string contains API KEY and the youtube user as parameters
	URL_DISCOVER_CHANNEL_ID = 'https://www.googleapis.com/youtube/v3/channels?key=' + this.GOOGLE_API_KEY + 
	'&forUsername=pretinhotv&part=id';

	URL_CHANNEL_VIDEOS = 'https://www.googleapis.com/youtube/v3/search?channelId=*value*&key=' + this.GOOGLE_API_KEY + '&maxResults=' + this.MAX_RESULT_OF_VIDEOS + '&order=date&part=id,snippet&q=&type=video';

	listVideos = [];

	nextPageToken = '';

  constructor(public navCtrl: NavController, public navParams: NavParams, 
    private http: Http, private sanitizer: DomSanitizer,
    private loadingCtrl: LoadingController, private youtube: YoutubeVideoPlayer) {

  	this.loadChannelVídeos();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VideosPage');
  }

  loadChannelVídeos() {
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: 'Carregando...'
    });

    loading.present();

  	this.http.get(this.URL_DISCOVER_CHANNEL_ID).map((res) => {
  		let channelId = res.json().items[0].id;
  		this.URL_CHANNEL_VIDEOS = this.URL_CHANNEL_VIDEOS.replace('*value*', channelId);

  		this.http.get(this.URL_CHANNEL_VIDEOS).map((res) => {
  			this.nextPageToken = res.json().nextPageToken;
	  		var rawlistVideos = res.json().items;
	  		for(var i = 0; i < rawlistVideos.length; i++) {
	  			this.listVideos.push({
            //url: this.sanatizeURL('https://www.youtube.com/embed/' + rawlistVideos[i].id.videoId)
	  				"videoId": rawlistVideos[i].id.videoId,
	  				"img": rawlistVideos[i].snippet.thumbnails.high.url
	  			})
	  		}

        loading.dismiss();
  		}).subscribe();

  	}).subscribe();
  }

  /*sanatizeURL(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }*/

  show5More() {
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: 'Carregando...'
    });

	  let urlWithNextPageToken = this.URL_CHANNEL_VIDEOS + '&pageToken=' + this.nextPageToken; 

  	this.http.get(urlWithNextPageToken).map((res) => {
  			this.nextPageToken = res.json().nextPageToken;
	  		var rawlistVideos = res.json().items;
	  		for(var i = 0; i < rawlistVideos.length; i++) {
	  			this.listVideos.push({
	  				"videoId": rawlistVideos[i].id.videoId,
            "img": rawlistVideos[i].snippet.thumbnails.high.url
	  			})
	  		}

        loading.dismiss();
  		}).subscribe();
  }

  openVideoOnYoutube(idVideo) {
    this.youtube.openVideo(idVideo);
  }
}