import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Component({
  selector: 'page-store',
  templateUrl: 'store.html',
})
export class StorePage {

	listProducts = [];

  URL_PRODUCTS = 'https://raw.githubusercontent.com/ruanpm/jsondb-pb-moments/master/data-shirts.json';

  constructor(public navCtrl: NavController, public navParams: NavParams, 
    private http: Http, private loadingCtrl: LoadingController) {

  	this.loadProducts();
  }

  loadProducts() {
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: 'Carregando...'
    });

    loading.present();

  	this.http.get(this.URL_PRODUCTS).map((res) => {
  		this.listProducts = res.json();
      loading.dismiss();
  	}).subscribe();
  }

  goToStore(url) {
    window.open(url, '_blank', 'location=no');
  }
}