import { Component } from '@angular/core';
import { StorePage } from '../store/store';
import { HomePage } from '../home/home';
import { VideosPage } from '../videos/videos';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = VideosPage;
  tab3Root = StorePage;

  constructor() {

  }
}
