import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';
import { Platform } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { AudioProvider } from 'ionic-audio';
import { SocialSharing } from '@ionic-native/social-sharing';
import { ActionSheetController } from 'ionic-angular';
import { AdMobPro } from '@ionic-native/admob-pro';


@Component({
	selector: 'page-home',
	templateUrl: 'home.html'
})
export class HomePage {
	dataPath = '';
	listPretinhos: string[];
	myTracks: any[];
 	allTracks: any[];
 	imgsPath = '';
 	shownGroup = null;

 	URL_DATA = 'https://raw.githubusercontent.com/ruanpm/jsondb-pb-moments/master/data.json';

	constructor(private _audioProvider: AudioProvider, private http: Http, public navCtrl: NavController, 
		//private streamingMedia: StreamingMedia, private webIntent: WebIntent
		private platform: Platform, private socialSharing: SocialSharing, private actionSheetCtrl: ActionSheetController,
		private admobPro: AdMobPro, private loadingCtrl: LoadingController) {

		this.loadAdMobBanner();

		this.createAudioList();

		var dataPath = this.platform.is('android') ? 'file:///android_asset/www/' : '';

		this.imgsPath = dataPath + 'assets/imgs/';
	}

	loadAdMobBanner() {
		this.admobPro.createBanner({
			 adId: 'ca-app-pub-1142746310231750/3731323730',
			 position: this.admobPro.AD_POSITION.BOTTOM_CENTER,
			 isTesting: false,
			 autoShow: true
		});
	}

	showAdMobInsterstitial() {
		this.admobPro.prepareInterstitial({
			adId: 'ca-app-pub-1142746310231750/2108208737'});
  }

	createAudioList() {
		let loading = this.loadingCtrl.create({
      		spinner: 'crescent',
      		content: 'Carregando...'
    	});

    	loading.present();

		//this.dataPath = this.dataPath + 'assets/mock/data.json';
		var self = this;
		return this.http.get(this.URL_DATA).map((res) => {
            self.listPretinhos = res.json();
            loading.dismiss();
        }).subscribe();
	}

	presentActionSheet(urlAudio) {
		var self = this;
	    let actionSheet = this.actionSheetCtrl.create({
	      title: 'Compartilhar',
	      buttons: [
	        {
	          text: 'WhatsApp',
	          role: 'destructive',
	          icon: 'ai-whatsapp',
	          handler: () => {
	           self.shareWhatsApp(urlAudio);
	          }
	        }/*,
	         {
	          text: 'Facebook',
	          role: 'destructive',
	          icon: 'custom-whatsapp',
	          handler: () => {
	           self.shareWhatsApp(urlAudio);
	          }
	        },
	         {
	          text: 'Instagram',
	          role: 'destructive',
	          icon: 'custom-whatsapp',
	          handler: () => {
	           self.shareWhatsApp(urlAudio);
	          }
	        }*/
	      ]
	    });
	    actionSheet.present();
  	}

	shareWhatsApp(urlAudio) {
		this.showAdMobInsterstitial();

		this.socialSharing.shareViaWhatsApp('audio', urlAudio, null).then(() => {
		  //console.log('well done');
		}).catch((err) => {
		 	console.log(err);
		});
	

		/*if ((<any>window).plugins) {
         	(<any>window).plugins.intentShim.startActivity(
                {
                    action: (<any>window).plugins.intentShim.ACTION_SEND,
                    //url: 'whatsapp://send?text=Hello'
                    //url: 'whatsapp://send',
                    type: 'text/plain',
                    extras: {
                    	'text': 'hehe'
                    }
                },
                function() {},
                function(err) {
                	console.log(err)
                }
            	);
			}
		}*/
	}

//OFICIAL

	ngAfterContentInit() {     
	    // get all tracks managed by AudioProvider so we can control playback via the API
	    this.allTracks = this._audioProvider.tracks; 
	}

	/*playSelectedTrack() {
	    // use AudioProvider to control selected track 
	    //this._audioProvider.play(selectedtrack);
	}

	pauseSelectedTrack() {
	     // use AudioProvider to control selected track 
	     //this._audioProvider.pause(selectedtrack);
	 }

	 onTrackFinished(track: any) {
	 	//console.log('Track finished', track)
	 }*/


	/*ionViewWillEnter() {
	    let tabs = document.querySelectorAll('.tabbar');
	    if ( tabs !== null ) {
	      Object.keys(tabs).map((key) => {
	        tabs[ key ].style.transform = 'translateY(56px)';
	      });
	    } // end if
	  }

	ionViewDidLeave() {
	    let tabs = document.querySelectorAll('.tabbar');
	    if ( tabs !== null ) {
	      Object.keys(tabs).map((key) => {
	        tabs[ key ].style.transform = 'translateY(0)';
	      });
	    } // end if
	}*/

	toggleGroup(group) {
	    if (this.isGroupShown(group)) {
	      this.shownGroup = null;
	    } else {
	      this.shownGroup = group;
	    }
	}
	
	isGroupShown(group) {
	    return this.shownGroup === group;
	};
}